$(document).ready(function(){

/*menu*/

$(".input-search").click(function(){
  $("#mySearch").toggle();
});

$(".menu").hide();
$("fa.times").hide();

$("#hamburger").click(function(){
  $("#hamburger").hide();
  $(".menu").fadeToggle();
  $(".fa-times").show();
  $(".previous").hide();
});

$(".fa-times").click(function(){
   $("#hamburger").show();
   $(".menu").hide();
   $(".fa-times").hide();
   $(".previous").show();
  });

var slideCounter = 0, // track slides
  $slides = $('#slider .slide'), // all the slides
  $slideDots = $('.page-dots .dots'), // all the slide dots
  tSlides = $slides.length, // total slides
  slideTimer,
  slideTimerLastRan=new Date().getTime();
  
  var showCurrentSlide = function(){
    var itemToShow = Math.abs(slideCounter%tSlides);
    slideCounter=itemToShow;
    $slides.removeClass('show');
    $slides.eq(itemToShow).addClass('show');    
    $slideDots.removeClass('is-selected');
    $slideDots.eq(itemToShow).addClass('is-selected');
  };
  
  var startSlideTimer=function(){
    slideTimer = setInterval(function(){ 
      slideTimerLastRan=new Date().getTime();
      slideCounter++;
      showCurrentSlide();
    },  3000);
  }
  var stopSlideTimer=function(){
    slideTimerLastRan=new Date().getTime();
    clearInterval(slideTimer);
    setTimeout(function(){
      var curTime=new Date().getTime();
      if(curTime-slideTimerLastRan>4000)
      {
        startSlideTimer();
      }
    },5000);
  }
  
  $('.next').on('click', function(){
    stopSlideTimer();
    slideCounter=(slideCounter==(tSlides-1)?0:slideCounter+1);
    showCurrentSlide();
  });
  $('.previous').on('click', function(){
    stopSlideTimer();
    slideCounter=(slideCounter==0?tSlides-1:slideCounter-1);
    showCurrentSlide();
  });
  
  $('.page-dots .dots').on('click', function(){
    stopSlideTimer();
    slideCounter=$(this).index();
    showCurrentSlide();
  });
  
  startSlideTimer();

});

/*Readmore--text*/
/*
  var showChar = 200;
  var ellipsesText = "[...]";
  var readLess = "[LESS]";
  var lessText;
  var moreText;
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      lessText =c;
      var h = '<span class="more-content">' + content.substr(showChar-1, content.length) + '&nbsp;</span>';
      moreText = h;
      var html = c + '<span class="moreEllipses">' + ellipsesText+ '&nbsp;</span>';

      $(this).html(html);
    }
  });
  
  $('.moreEllipses').click(function(){
    var html = moreText + '<span class="lessContent">' + readLess + '&nbsp;</span>';
      $(this).html(html);
  });
  $('.fa-search').click(function(){
    $('#mySearch').toggle();
  });

*/


