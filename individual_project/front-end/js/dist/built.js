$( document ).ready(function() {

manageData();

/* manage data list, get data*/
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/destination/travel.jsp'
    }).done(function(data){
        manageRow(data);
    });

}

/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.destination+'</td>';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.image+'</td>';
        rows = rows + '<td>'+value.date+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });

    $("tbody").html(rows);
}
/*Create New ite'm*/
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var destination= $("#create-item").find("input[name='destination']").val();
    var description = $("#create-item").find("textarea[name='description']").val();
    var image= $("#create-item").find("input[name='image']").val();
    var date= $("#create-item").find("input[name='date']").val();

    if(destination != ''){
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: form_action,
            data:{destination:destination, description:description, image:image, date:date}
        }).done(function(data){
            $("#create-item").find("input[name='destination']").val('');
            $("#create-item").find("textarea[name='description']").val('');
            $("#create-item").find("input[name='image']").val('');
            $("#create-item").find("input[name='date']").val('');
            $(".modal").modal('hide');
            manageData();
            console.log(data);
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});

        });
    }else{
        alert('Something is missing.')
    }

});
/* Remove Item */
$("body").on("click",".remove-item",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/destination/delete.jsp',
        data:{id:id}
    }).done(function(data){
        c_obj.remove();
        toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 3000});
    });

});
/* Edit Item get travels by id*/
$("body").on("click",".edit-item",function(){
   
    var id = $(this).parent("td").data('id');

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/destination/getTravelByid.jsp',
        data:{id:id}
    }).done(function(data){

        $("#edit-item").find("input[name='destination']").val(data[0].destination);
        $("#edit-item").find("textarea[name='description']").val(data[0].description);
        $("#edit-item").find("input[name='image']").val(data[0].image);
        $("#edit-item").find("input[name='date']").val(data[0].date);
        $("#edit-item").find("input[name='id']").val(data[0].id);

    });

});
    /* Updated new Item --save*/
    $(".crud-submit-edit").click(function(e){

         e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var destination = $("#edit-item").find("input[name='destination']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();
        var image = $("#edit-item").find("input[name='image']").val();
        var date = $("#edit-item").find("input[name='date']").val();
        var id = $("#edit-item").find("input[name='id']").val();

        if(destination != '' && description != ''){
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: form_action,
                data:{destination:destination,description:description,image:image, date:date, id:id}
            }).done(function(data){
                manageData(); 
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 3000});
            });
        }else{
            alert('You are missing some information.')
        }
    });
});
$( document ).ready(function() {

manageData();

/* manage data list, get data*/
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/travel-tips/tips.jsp'
    }).done(function(data){
        manageRow(data);
    });

}

/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.title+'</td>';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.image+'</td>';
        rows = rows + '<td>'+value.date+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });

    $("tbody").html(rows);
}
/*Create New ite'm*/
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var title= $("#create-item").find("input[name='title']").val();
    var description = $("#create-item").find("textarea[name='description']").val();
    var image= $("#create-item").find("input[name='image']").val();
    var date= $("#create-item").find("input[name='date']").val();

    if(title != ''){
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: form_action,
            data:{title:title, description:description, image:image, date:date}
        }).done(function(data){
            $("#create-item").find("input[name='title']").val('');
            $("#create-item").find("textarea[name='description']").val('');
            $("#create-item").find("input[name='image']").val('');
            $("#create-item").find("input[name='date']").val('');
            $(".modal").modal('hide');
            manageData();
            console.log(data);
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});

        });
    }else{
        alert('Something is missing.')
    }

});
/* Remove Item */
$("body").on("click",".remove-item",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/travel-tips/delete.jsp',
        data:{id:id}
    }).done(function(data){
        c_obj.remove();
        toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 3000});
    });

});
/* Edit Item get travels by id*/
$("body").on("click",".edit-item",function(){
   
    var id = $(this).parent("td").data('id');

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/travel-tips/getTipsByid.jsp',
        data:{id:id}
    }).done(function(data){

        $("#edit-item").find("input[name='title']").val(data[0].title);
        $("#edit-item").find("textarea[name='description']").val(data[0].description);
        $("#edit-item").find("input[name='image']").val(data[0].image);
        $("#edit-item").find("input[name='date']").val(data[0].date);
        $("#edit-item").find("input[name='id']").val(data[0].id);

    });

});
    /* Updated new Item --save*/
    $(".crud-submit-edit").click(function(e){

         e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var title = $("#edit-item").find("input[name='title']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();
        var image = $("#edit-item").find("input[name='image']").val();
        var date = $("#edit-item").find("input[name='date']").val();
        var id = $("#edit-item").find("input[name='id']").val();

        if(title != '' && description != ''){
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: form_action,
                data:{title:title,description:description,image:image, date:date, id:id}
            }).done(function(data){
                manageData(); 
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 3000});
            });
        }else{
            alert('You are missing some information.')
        }
    });
});
$( document ).ready(function() {

    $("#signup-form").hide();

    $(".container .selection-wrap #tab-1").click(function(){
        $("#signin-form").show();
        $("#signup-form").hide();
    });
    $(".container .selection-wrap #tab-2").click(function(){
        $("#signup-form").show();
        $("#signin-form").hide();
    });

    $("#signin-submit").click(function(e){

        e.preventDefault(); 
        var form_action = $("#login").attr("action");
        var username = $("#login-form").find("input[name='username']").val();
        var password = $("#login-form").find("input[name='password']").val();   
        if(username != '' && password != ''){
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: form_action,
                data:{username:username, password:password}         
            }).done(function(data){
            window.console(data["sessionId"]);
                            //SAVE COOKIE
                if (data["sessionId"]) {
                    var cookieValue = $.cookie("sessionId", data["sessionId"]);
                    console.log(cookieValue);
                    console.log(sessionId);
                    window.location.href="index.html";
                } else {

                   $("#loginError").show();
                }
            });        
        }
    });

});
    
$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/travel-tips/tips.jsp'
    }).done(function(data){
    	manageRow(data);
    });

}


/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        
        rows = rows + '<div class="entry-body">';
        rows = rows + '<img class="animate-img" src="' + value.image + '">';
        rows = rows + '<div class="caption">';
        rows = rows + '<span class="line">' + value.date +'</span>';
        rows = rows + '<span class="line">& 1 comment</span>';
        rows = rows + '<h1>' + value.title + '</h1>';
        rows = rows + '</div>';
        rows = rows + '</div>';
    });

    $(".section-five .tips").html(rows);
}


});
$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/destination/travel.jsp'
    }).done(function(data){
    	manageRow(data);
    });

}


/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        
        rows = rows + '<div class="entry-body">';
        rows = rows + '<img class="animate-img" src="' + value.image + '">';
        rows = rows + '<div class="caption">';
        rows = rows + '<span class="line">' + value.date +'</span>';
        rows = rows + '<span class="line">& 1 comment</span>';
        rows = rows + '<h1>' + value.destination + '</h1>';
        rows = rows + '</div>';
        rows = rows + '</div>';
    });
    console.log(rows);
    $(".section-three .travel").html(rows);
}

});


$(document).ready(function(){

/*menu*/

$(".input-search").click(function(){
  $("#mySearch").toggle();
});

$(".menu").hide();
$("fa.times").hide();

$("#hamburger").click(function(){
  $("#hamburger").hide();
  $(".menu").fadeToggle();
  $(".fa-times").show();
  $(".previous").hide();
});

$(".fa-times").click(function(){
   $("#hamburger").show();
   $(".menu").hide();
   $(".fa-times").hide();
   $(".previous").show();
  });

var slideCounter = 0, // track slides
  $slides = $('#slider .slide'), // all the slides
  $slideDots = $('.page-dots .dots'), // all the slide dots
  tSlides = $slides.length, // total slides
  slideTimer,
  slideTimerLastRan=new Date().getTime();
  
  var showCurrentSlide = function(){
    var itemToShow = Math.abs(slideCounter%tSlides);
    slideCounter=itemToShow;
    $slides.removeClass('show');
    $slides.eq(itemToShow).addClass('show');    
    $slideDots.removeClass('is-selected');
    $slideDots.eq(itemToShow).addClass('is-selected');
  };
  
  var startSlideTimer=function(){
    slideTimer = setInterval(function(){ 
      slideTimerLastRan=new Date().getTime();
      slideCounter++;
      showCurrentSlide();
    },  3000);
  }
  var stopSlideTimer=function(){
    slideTimerLastRan=new Date().getTime();
    clearInterval(slideTimer);
    setTimeout(function(){
      var curTime=new Date().getTime();
      if(curTime-slideTimerLastRan>4000)
      {
        startSlideTimer();
      }
    },5000);
  }
  
  $('.next').on('click', function(){
    stopSlideTimer();
    slideCounter=(slideCounter==(tSlides-1)?0:slideCounter+1);
    showCurrentSlide();
  });
  $('.previous').on('click', function(){
    stopSlideTimer();
    slideCounter=(slideCounter==0?tSlides-1:slideCounter-1);
    showCurrentSlide();
  });
  
  $('.page-dots .dots').on('click', function(){
    stopSlideTimer();
    slideCounter=$(this).index();
    showCurrentSlide();
  });
  
  startSlideTimer();

});

/*Readmore--text*/
/*
  var showChar = 200;
  var ellipsesText = "[...]";
  var readLess = "[LESS]";
  var lessText;
  var moreText;
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      lessText =c;
      var h = '<span class="more-content">' + content.substr(showChar-1, content.length) + '&nbsp;</span>';
      moreText = h;
      var html = c + '<span class="moreEllipses">' + ellipsesText+ '&nbsp;</span>';

      $(this).html(html);
    }
  });
  
  $('.moreEllipses').click(function(){
    var html = moreText + '<span class="lessContent">' + readLess + '&nbsp;</span>';
      $(this).html(html);
  });
  $('.fa-search').click(function(){
    $('#mySearch').toggle();
  });

*/


