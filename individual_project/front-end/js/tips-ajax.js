$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/travel-tips/tips.jsp'
    }).done(function(data){
    	manageRow(data);
    });

}


/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        
        rows = rows + '<div class="entry-body">';
        rows = rows + '<img class="animate-img" src="' + value.image + '">';
        rows = rows + '<div class="caption">';
        rows = rows + '<span class="line">' + value.date +'</span>';
        rows = rows + '<span class="line">& 1 comment</span>';
        rows = rows + '<h1>' + value.title + '</h1>';
        rows = rows + '</div>';
        rows = rows + '</div>';
    });

    $(".section-five .tips").html(rows);
}


});