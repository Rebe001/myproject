$( document ).ready(function() {

manageData();

/* manage data list, get data*/
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/individual_project/JSP/destination/travel.jsp'
    }).done(function(data){
        manageRow(data);
    });

}

/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.destination+'</td>';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.image+'</td>';
        rows = rows + '<td>'+value.date+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });

    $("tbody").html(rows);
}
/*Create New ite'm*/
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var destination= $("#create-item").find("input[name='destination']").val();
    var description = $("#create-item").find("textarea[name='description']").val();
    var image= $("#create-item").find("input[name='image']").val();
    var date= $("#create-item").find("input[name='date']").val();

    if(destination != ''){
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: form_action,
            data:{destination:destination, description:description, image:image, date:date}
        }).done(function(data){
            $("#create-item").find("input[name='destination']").val('');
            $("#create-item").find("textarea[name='description']").val('');
            $("#create-item").find("input[name='image']").val('');
            $("#create-item").find("input[name='date']").val('');
            $(".modal").modal('hide');
            manageData();
            console.log(data);
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});

        });
    }else{
        alert('Something is missing.')
    }

});
/* Remove Item */
$("body").on("click",".remove-item",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/destination/delete.jsp',
        data:{id:id}
    }).done(function(data){
        c_obj.remove();
        toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 3000});
    });

});
/* Edit Item get travels by id*/
$("body").on("click",".edit-item",function(){
   
    var id = $(this).parent("td").data('id');

    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'http://localhost:8080/individual_project/JSP/destination/getTravelByid.jsp',
        data:{id:id}
    }).done(function(data){

        $("#edit-item").find("input[name='destination']").val(data[0].destination);
        $("#edit-item").find("textarea[name='description']").val(data[0].description);
        $("#edit-item").find("input[name='image']").val(data[0].image);
        $("#edit-item").find("input[name='date']").val(data[0].date);
        $("#edit-item").find("input[name='id']").val(data[0].id);

    });

});
    /* Updated new Item --save*/
    $(".crud-submit-edit").click(function(e){

         e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var destination = $("#edit-item").find("input[name='destination']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();
        var image = $("#edit-item").find("input[name='image']").val();
        var date = $("#edit-item").find("input[name='date']").val();
        var id = $("#edit-item").find("input[name='id']").val();

        if(destination != '' && description != ''){
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: form_action,
                data:{destination:destination,description:description,image:image, date:date, id:id}
            }).done(function(data){
                manageData(); 
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 3000});
            });
        }else{
            alert('You are missing some information.')
        }
    });
});