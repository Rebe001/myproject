module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
                js:{
                    src: [
                        'front-end/js/*.js',
                        'front-end/javascript.js'
                    ],
                    dest: 'front-end/js/dist/built.js',
                    },
                css:{
                    src: 'front-end/*.css',
                    dest: 'front-end/css/dist/built.css',
                    }
        },

        uglify: {
            build: {
                src: 'front-end/js/dist/built.js',
                dest: 'front-end/js/dist/built.min.js'
             }
        },

       imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    src: ['**/*.{jpg,jpeg,JPG}'],
                    dest: 'front-end/images/build/'
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                  src: 'front-end/css/dist/built.css',
                  dest: 'front-end/css/dist/built.min.css',
                }]
            }          
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');


    grunt.registerTask('default', ['concat', 'uglify', 'imagemin','cssmin']);


};