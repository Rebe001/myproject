-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2017 at 12:31 PM
-- Server version: 5.7.16-log
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE `tips` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id`, `title`, `description`, `image`, `date`) VALUES
(1, 'Brazil, 24 hours non-stop', '  12345Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima.', 'images/build/front-end/photo/Brazil.jpg', '2016-12-15'),
(2, 'Survival Kit in Beijing', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima.', 'images/build/front-end/photo/Beijing.jpg', '2016-12-15'),
(3, 'Paris, Morning Breeze', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima.', 'images/build/front-end/photo/Paris.jpg', '2016-12-04');

-- --------------------------------------------------------

--
-- Table structure for table `travels`
--

CREATE TABLE `travels` (
  `id` int(11) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `travels`
--

INSERT INTO `travels` (`id`, `destination`, `description`, `image`, `date`) VALUES
(5, 'Iceland, Bath in blue lagoon', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima.', 'images/build/front-end/photo/Iceland1.jpg', '2016-03-16'),
(9, 'Prague, Filled with romance', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima.', 'images/build/front-end/photo/Prague1.jpg', '2016-06-01'),
(13, 'Brazil, Soak in pink beach', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui aspernatur sunt maxime dolorum ratione, inventore iure, voluptatem autem minus excepturi! Cupiditate fugiat mollitia dolor quisquam tenetur dolorum maxime, dicta recusandae!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est officia exercitationem eius deserunt adipisci hic sunt, nostrum iure ad, unde, sapiente eaque delectus expedita voluptatum! Maiores, temporibus eos alias.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur quos a, corrupti possimus minus, mollitia rerum repudiandae, dolor commodi sapiente laudantium unde tenetur nostrum est quod nam fugit consectetur minima', 'images/build/front-end/photo/Brazil2.jpg', '2015-08-04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUser`, `username`, `password`) VALUES
(11, 'tester1', '72a3dcef165d9122a45decf13ae20631');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `travels`
--
ALTER TABLE `travels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `travels`
--
ALTER TABLE `travels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
